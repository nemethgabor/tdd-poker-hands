package poker.hand.matcher.domain;

public enum HandScore {
    STRAIGHT_FLUSH(9),
    FOUR_OF_KIND(8),
    FULL_HOUSE(7),
    FLUSH(6),
    STRAIGHT(5),
    THREE_OF_KIND(4),
    TWO_PAIR(3),
    PAIR(2),
    HIGH_CARD(1);

    private final int scoreAsInt;

    HandScore(int scoreAsInt) {
        this.scoreAsInt = scoreAsInt;
    }
}
