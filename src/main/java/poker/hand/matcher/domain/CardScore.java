package poker.hand.matcher.domain;

import lombok.Getter;

public enum CardScore {

    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JUMBO(11),
    QUEEN(12),
    KING(13),
    ACE(14);

    @Getter
    private final int realValue;

    CardScore(int realScore) {
        this.realValue = realScore;
    }

    public static CardScore getByString(String scoreAsString) {
        if(isScoreAsStringADigit(scoreAsString)){
            int scoreAsInteger = Integer.parseInt(scoreAsString);
            return getCardScoreAsRealValue(scoreAsInteger);
        }
        switch (scoreAsString){
            case "J": return getCardScoreAsRealValue(11);
            case "Q": return getCardScoreAsRealValue(12);
            case "K": return getCardScoreAsRealValue(13);
            case "A": return getCardScoreAsRealValue(14);
        }
        return null;
    }

    private static CardScore getCardScoreAsRealValue(int scoreAsInteger) {
        for (CardScore cardScore : CardScore.values()) {
            if(cardScore.realValue == scoreAsInteger){
                return cardScore;
            }
        }
        return null;
    }

    private static boolean isScoreAsStringADigit(String scoreAsString) {
        try {
            Integer.parseInt(scoreAsString);
            return true;
        } catch (NumberFormatException exception){
            return false;
        }
    }

    @Override
    public String toString() {
        return String.valueOf(realValue);
    }
}
