package poker.hand.matcher.domain;

import lombok.Getter;
import poker.hand.matcher.domain.exceptions.SameCardException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Hand {

    // S - pikk (spades) ♠
    // H - szív (heart) ♥
    // D - káró (diamond) ♦
    // C - treff (club) ♣

    @Getter
    private final List<Card> allCards = new ArrayList<>();

    public Hand(String card1, String card2, String card3, String card4, String card5) {
        collectCards(card1, card2, card3, card4, card5);
        validateForSameCard();
    }

    private void validateForSameCard() {
        Set<String> allCardsValidate = new HashSet<>();
        allCards.stream().map(Card::toString).forEach(allCardsValidate::add);
        if(allCardsValidate.size() != 5){
            throw new SameCardException();
        }
    }


    private void collectCards(String card1, String card2, String card3, String card4, String card5) {
        allCards.add(new Card(card1));
        allCards.add(new Card(card2));
        allCards.add(new Card(card3));
        allCards.add(new Card(card4));
        allCards.add(new Card(card5));
    }




}
