package poker.hand.matcher.domain;

import lombok.Data;
import poker.hand.matcher.domain.exceptions.InvalidCardException;
import poker.hand.matcher.domain.exceptions.InvalidCardScoreException;
import poker.hand.matcher.domain.exceptions.InvalidCardTypeException;

import java.util.Arrays;
import java.util.List;

@Data
public class Card {

    private static final List<String> cardTypes = Arrays.asList("S", "H", "D", "C");

    private CardScore score;
    private String type;

    public Card(String cardAsString) {
        makeCard(cardAsString);
        validateCard();
    }

    private void makeCard(String card) {
        if (card == null || card.length() < 2 || card.length() > 3) {
            throw new InvalidCardException();
        } else if (card.length() == 2) {
            getBy2Length(card);
        } else {
            getBy3Length(card);
        }
    }

    private void getBy3Length(String card) {
        score = CardScore.getByString(card.substring(0, 2));
        type = card.substring(2, 3);
    }
    private void getBy2Length(String card) {
        score = CardScore.getByString(card.substring(0, 1));
        type = card.substring(1, 2);
    }

    private void validateCard() {
        if (!cardTypes.contains(type)) {
            throw new InvalidCardTypeException();
        }
        if (score == null) {
            throw new InvalidCardScoreException();
        }
    }

    @Override
    public String toString() {
        return score.toString() + type;
    }
}
