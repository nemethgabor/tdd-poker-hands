package poker.hand.matcher.service;

import poker.hand.matcher.domain.Card;
import poker.hand.matcher.domain.CardScore;
import poker.hand.matcher.domain.Hand;
import poker.hand.matcher.domain.HandScore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HandEvaluate {
    public static HandScore evaluate(Hand hand) {
        if (handAreStraitFlush(hand)) {
            return HandScore.STRAIGHT_FLUSH;
        } else if (handAreFourOfKind(hand)) {
            return HandScore.FOUR_OF_KIND;
        } else if (handAreStrait(hand)) {
            return HandScore.STRAIGHT;
        } else if (handAreFlush(hand)) {
            return HandScore.FLUSH;
        } else if (handAreFullHouse(hand)) {
            return HandScore.FULL_HOUSE;
        } else if (handAreThreeOfKind(hand)) {
            return HandScore.THREE_OF_KIND;
        } else if (numberOfPairs(hand) == 2) {
            return HandScore.TWO_PAIR;
        } else if (numberOfPairs(hand) == 1) {
            return HandScore.PAIR;
        }
        return HandScore.HIGH_CARD;
    }

    private static int numberOfPairs(Hand hand) {
        List<Integer> pairs = new ArrayList<>();
        for (Card c : hand.getAllCards()) {
            if (!pairs.contains(c.getScore().getRealValue()) && hasPairWithThisCard(hand, c)) {
                pairs.add(c.getScore().getRealValue());
            }
        }
        return pairs.size();
    }

    private static boolean handAreFullHouse(Hand hand) {
        List<Integer> counts = collectCardsAsScore(hand);
        return counts.contains(2) && counts.contains(3);
    }

    private static boolean hasPairWithThisCard(Hand hand, Card card) {
        for (Card c : hand.getAllCards()) {
            if (!c.equals(card) && c.getScore() == card.getScore()) {
                return true;
            }
        }
        return false;
    }

    private static boolean handAreThreeOfKind(Hand hand) {
        for (Card card : hand.getAllCards()) {
            if (isHaveTreeOfKindForThisCard(hand, card)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isHaveTreeOfKindForThisCard(Hand hand, Card card) {
        int count = 0;
        for (Card c : hand.getAllCards()) {
            if (card.getScore().getRealValue() == c.getScore().getRealValue()) {
                count++;
            }
            if (count == 3) {
                return true;
            }
        }
        return false;
    }

    private static boolean handAreFourOfKind(Hand hand) {
        List<Integer> counts = collectCardsAsScore(hand);
        return counts.contains(1) && counts.contains(4);
    }

    private static List<Integer> collectCardsAsScore(Hand hand) {
        Map<Integer, Integer> cardCount = new HashMap<>();
        for (Card card : hand.getAllCards()) {
            if (cardCount.containsKey(card.getScore().getRealValue())) {
                cardCount.put(card.getScore().getRealValue(), cardCount.get(card.getScore().getRealValue()) + 1);
            } else {
                cardCount.put(card.getScore().getRealValue(), 1);
            }
        }
        return cardCount.values().stream().sorted().collect(Collectors.toList());
    }

    private static boolean handAreStraitFlush(Hand hand) {
        if (handAreFlush(hand)) { // ugyanolyan színű
            return isInConsecutiveOrder(hand);
        }
        return false;
    }

    private static boolean handAreFlush(Hand hand) {
        int size = hand.getAllCards().stream().map(Card::getType).collect(Collectors.toSet()).size();
        return size == 1;
    }

    private static boolean handAreStrait(Hand hand) {
        return isInConsecutiveOrder(hand);
    }

    private static boolean isInConsecutiveOrder(Hand hand) {
        List<Integer> realValuesOfCards = hand.getAllCards()
                .stream()
                .map(Card::getScore)
                .map(CardScore::getRealValue)
                .sorted()
                .collect(Collectors.toList());

        int number = 0;
        for (Integer realValue : realValuesOfCards) {
            if (number == 0) {
                number = realValue;
                number++;
            } else {
                if (number == realValue) {
                    number++;
                } else {
                    return realValue == 14 && number == 6;
                }
            }
        }
        return true;
    }
}
