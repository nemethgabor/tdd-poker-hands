package poker.hand.matcher.domain;

import org.junit.jupiter.api.Test;
import poker.hand.matcher.domain.exceptions.InvalidCardException;
import poker.hand.matcher.domain.exceptions.InvalidCardScoreException;
import poker.hand.matcher.domain.exceptions.InvalidCardTypeException;
import poker.hand.matcher.domain.exceptions.SameCardException;

import static org.junit.jupiter.api.Assertions.*;

public class HandTest {

    @Test
    public void addNullCardTest(){
        assertThrows(InvalidCardException.class, () -> new Hand(null, "3S", "4S", "5S", "PS"));
    }

    @Test
    public void addEmptyCardTest(){
        assertThrows(InvalidCardException.class, () -> new Hand("", "3S", "4S", "5S", "PS"));
    }

    @Test
    public void addInvalidTypeLengthTest(){
        assertThrows(InvalidCardException.class, () -> new Hand("22SS", "3S", "4S", "5S", "6Q"));
    }

    @Test
    public void addInvalidTypeCardTest(){
        assertThrows(InvalidCardTypeException.class, () -> new Hand("2S", "3S", "4S", "5S", "6Q"));
    }

    @Test
    public void addInvalidScoreCardTest(){
        assertThrows(InvalidCardScoreException.class, () -> new Hand("2S", "3S", "4S", "5S", "PS"));
    }

    @Test
    public void addSameCardAndGotExceptionTest(){
        assertThrows(SameCardException.class, () -> new Hand("2S", "2S", "4S", "5S", "6S"));
    }

}
