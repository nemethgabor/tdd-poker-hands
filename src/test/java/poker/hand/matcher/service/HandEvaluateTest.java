package poker.hand.matcher.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import poker.hand.matcher.domain.Hand;
import poker.hand.matcher.domain.HandScore;

public class HandEvaluateTest {

    @Test
    public void testForStraightFlush() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("10S", "JS", "QS", "KS", "AS"));
        Assertions.assertEquals(HandScore.STRAIGHT_FLUSH, handScore);
    }

    @Test
    public void testForStraightFlushFrom5() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("5S", "6S", "7S", "8S", "9S"));
        Assertions.assertEquals(HandScore.STRAIGHT_FLUSH, handScore);
    }

    @Test
    public void testForStraightFlushFromAce() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("AS", "2S", "3S", "4S", "5S"));
        Assertions.assertEquals(HandScore.STRAIGHT_FLUSH, handScore);
    }

    @Test
    public void testForStraightFlushUnorderedHand() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("9S", "10S", "JS", "KS", "QS"));
        Assertions.assertEquals(HandScore.STRAIGHT_FLUSH, handScore);
    }

    @Test
    public void testForFourOfKindFourAce() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("AS", "AH", "AD", "AC", "2S"));
        Assertions.assertEquals(HandScore.FOUR_OF_KIND, handScore);
    }

    @Test
    public void testForFourOfKindFourNumber4() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("4S", "4H", "4D", "4C", "2S"));
        Assertions.assertEquals(HandScore.FOUR_OF_KIND, handScore);
    }

    @Test
    public void testForThreeOfKindThreeAce() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("AS", "AH", "AD", "3C", "2S"));
        Assertions.assertEquals(HandScore.THREE_OF_KIND, handScore);
    }

    @Test
    public void testForFourOfKindFourNumber3() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("4S", "4H", "4D", "3C", "2S"));
        Assertions.assertEquals(HandScore.THREE_OF_KIND, handScore);
    }

    @Test
    public void testForTwoPair() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("4S", "4H", "2D", "2C", "3S"));
        Assertions.assertEquals(HandScore.TWO_PAIR, handScore);
    }

    @Test
    public void testForTwoPairTwoAceTwoKing() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("AS", "AH", "KD", "KC", "2S"));
        Assertions.assertEquals(HandScore.TWO_PAIR, handScore);
    }

    @Test
    public void testForStraight() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("2S", "3H", "4D", "5C", "6S"));
        Assertions.assertEquals(HandScore.STRAIGHT, handScore);
    }

    @Test
    public void testForStraightWithAceOnLeft() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("AS", "2H", "3D", "4C", "5S"));
        Assertions.assertEquals(HandScore.STRAIGHT, handScore);
    }

    @Test
    public void testForStraightWithAceOnRight() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("5S", "2H", "3D", "4C", "AS"));
        Assertions.assertEquals(HandScore.STRAIGHT, handScore);
    }

    @Test
    public void testForStraightWithAceOnTheEnd() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("10H", "JD", "KC", "QS", "AS"));
        Assertions.assertEquals(HandScore.STRAIGHT, handScore);
    }

    @Test
    public void testForFlushWithTypeDiamond() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("5D", "6D", "7D", "2D", "9D"));
        Assertions.assertEquals(HandScore.FLUSH, handScore);
    }

    @Test
    public void testForFlushWithTypeHeart() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("5H", "6H", "7H", "2H", "9H"));
        Assertions.assertEquals(HandScore.FLUSH, handScore);
    }

    @Test
    public void testForFullHouse() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("5H", "5S", "3C", "3D", "3H"));
        Assertions.assertEquals(HandScore.FULL_HOUSE, handScore);
    }

    @Test
    public void testForFullHouseWithAcePairAndKingDrill() {
        HandScore handScore = HandEvaluate.evaluate(new Hand("AH", "AS", "KH", "KD", "KC"));
        Assertions.assertEquals(HandScore.FULL_HOUSE, handScore);
    }

    //TODO test for two pair, pair,straigt,fullhouse,highcard,flush

}
